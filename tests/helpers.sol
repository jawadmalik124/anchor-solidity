// SPDX-License-Identifier: GPL-3.0
        
pragma solidity >=0.4.22 <0.9.0;

library helpers {

        // Convert an hexadecimal character to their value
    function fromHexChar(uint8 c) public pure returns (uint8) {
        uint8 ret = 0;
        if (bytes1(c) >= bytes1('0') && bytes1(c) <= bytes1('9')) {
            ret = c - uint8(bytes1('0'));
        }
        else if (bytes1(c) >= bytes1('a') && bytes1(c) <= bytes1('f')) {
            ret = 10 + c - uint8(bytes1('a'));
        }
        else if (bytes1(c) >= bytes1('A') && bytes1(c) <= bytes1('F')) {
            ret = 10 + c - uint8(bytes1('A'));
        }
        return ret;
    }

    // Convert an hexadecimal string to raw bytes
    function hex2bytes(string memory s) public pure returns (bytes memory) {
        bytes memory ss = bytes(s);
        require(ss.length%2 == 0); // length must be even
        bytes memory r = new bytes(ss.length/2);
        for (uint i=0; i<ss.length/2; ++i) {
            r[i] = bytes1(fromHexChar(uint8(ss[2*i])) * 16 +
                        fromHexChar(uint8(ss[2*i+1])));
        }
        return r;
    }

    function bytesToBytes32(bytes memory b) public pure returns (bytes32) {
        bytes32 out;
        uint offset = 0;
        for (uint i = 0; i < 32; i++) {
            out |= bytes32(b[offset + i] & 0xFF) >> (i * 8);
        }
        return out;
    }

    function hex2bytes32(string memory s) public pure returns (bytes32) {
        return bytesToBytes32(hex2bytes(s));
    }

}    

